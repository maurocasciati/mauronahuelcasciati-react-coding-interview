import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Input, message, Form, Checkbox } from 'antd';

import { withContextInitialized } from '../../../components/hoc';
import CompanyCard from '../../../components/molecules/CompanyCard';
import GenericList from '../../../components/organisms/GenericList';
import OverlaySpinner from '../../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../../components/hooks/usePersonInformation';

import { Company, Person } from '../../../constants/types';
import { ResponsiveListCard } from '../../../constants';

const PersonEdit = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const onFinish = (newData: Person) => {
    save({
      ...data,
      ...newData
    })
    router.back()
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => {}}>
            Edit
          </Button>,
        ]}
      >
        {data && (
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={data}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Name"
              name="name"
              rules={[{ required: true, message: 'Please input name' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Gender"
              name="gender"
              rules={[{ required: true, message: 'Please input gender' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Phone"
              name="phone"
              rules={[{ required: true, message: 'Please input phone' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Birthday"
              name="birthday"
              rules={[{ required: true, message: 'Please input birthday' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        )}
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonEdit);
